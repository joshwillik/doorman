const child_process = require('child_process')
const secrets = require('./secrets.json')
function run(cmd) {
  console.log(cmd)
  child_process.execSync(cmd, {stdio: ['inherit', 'inherit', 'inherit']})
}
run(`now rm -y ${process.env.npm_package_name || 'nothing'}`)
run(`now --public -e TWILIO_TOKEN=${secrets.twilio_token}`)
