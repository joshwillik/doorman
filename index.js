module.exports = async (req, res) => {
  if (req.method !== 'POST') {
    res.writeHead(404)
    res.end()
    return
  }
  res.writeHead(200, {'Content-Type': 'text/xml'})
  res.end(`<?xml version="1.0" encoding="UTF-8"?>
    <Response><Play digits="ww9w"/></Response>`)
}
